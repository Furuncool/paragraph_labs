import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  MongooseHealthIndicator,
} from '@nestjs/terminus';
import { ApiOperation } from '@nestjs/swagger';

@Controller('health')
export class HealthController {
  constructor(
    private healthCheckService: HealthCheckService,
    private mongooseHealthIndicator: MongooseHealthIndicator,
  ) {}

  @ApiOperation({
    summary: 'Health check',
  })
  @Get()
  @HealthCheck()
  check(): Promise<HealthCheckResult> {
    return this.healthCheckService.check([
      () => this.mongooseHealthIndicator.pingCheck('database'),
    ]);
  }
}
