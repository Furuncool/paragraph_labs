import { Document, now } from 'mongoose';
import { Prop } from '@nestjs/mongoose';

/**
 * The base entity for MongoDB with _id and timestamps
 */
export class BaseEntity extends Document {
  @Prop({ default: now() })
  createdAt: Date;

  @Prop({ default: now() })
  updatedAt: Date;
}
