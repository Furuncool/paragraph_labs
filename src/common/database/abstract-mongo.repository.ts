import {
  Document,
  FilterQuery,
  Model,
  QueryOptions,
  UpdateQuery,
  UpdateWithAggregationPipeline,
} from 'mongoose';
import { IMongoPaginationDto, PaginationMeta } from '../dtos';
import { PopulatesParams } from '../common.types';

export abstract class AbstractMongoRepository<T extends Document> {
  protected constructor(private readonly model: Model<T>) {}

  async create(doc: object, populates?: PopulatesParams): Promise<T> {
    const createdEntity = new this.model(doc);
    if (populates) {
      await createdEntity.populate(populates);
    }
    return createdEntity.save();
  }

  async findOne(
    filter: FilterQuery<T>,
    options?: QueryOptions<T>,
  ): Promise<T | null> {
    return this.model.findOne(filter, null, options);
  }

  async find(
    filter?: FilterQuery<T>,
    options?: QueryOptions<T>,
  ): Promise<IMongoPaginationDto<T>> {
    const data = await this.model.find(filter, null, options);
    const currentCount = data.length;
    const totalCount = await this.model.count(filter);

    const paginationMeta = PaginationMeta.create(
      currentCount,
      totalCount,
      options.skip,
      options.limit,
    );

    return {
      data,
      ...paginationMeta,
    };
  }

  async update(
    filter: FilterQuery<T>,
    update: UpdateWithAggregationPipeline | UpdateQuery<T>,
    options?: QueryOptions<T>,
  ): Promise<T | null> {
    return this.model.findOneAndUpdate(filter, update, {
      ...options,
      new: true,
    });
  }

  async delete(
    filter: FilterQuery<T>,
    options?: QueryOptions<T>,
  ): Promise<T | null> {
    return this.model.findOneAndDelete(filter, options);
  }
}
