import { PopulateOptions } from 'mongoose';

export enum ENVIRONMENT {
  DEVELOPMENT = 'development',
  STAGING = 'staging',
  PRODUCTION = 'production',
}

export enum SortDirection {
  'ASCENDING' = 'asc',
  'DESCENDING' = 'desc',
}

export type PopulatesParams =
  | string
  | string[]
  | PopulateOptions
  | PopulateOptions[];
