import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { ConfigService } from '@nestjs/config';

type FileActions = 'putObject' | 'getObject';

@Injectable()
export class S3Service {
  constructor(private readonly configService: ConfigService) {
    this.client = this.createS3FromConfig();
  }

  private readonly client: AWS.S3;

  private createS3FromConfig(): AWS.S3 {
    return new AWS.S3({
      signatureVersion: 'v4',
      region: this.configService.get<string>('s3.region'),
      credentials: {
        accessKeyId: this.configService.get<string>('s3.key'),
        secretAccessKey: this.configService.get<string>('s3.secret'),
      },
      s3ForcePathStyle: true,
      sslEnabled: false,
      endpoint: this.configService.get<string>('s3.endpoint'),
    });
  }

  private async getSignedUrl(
    bucketName: string,
    fileKey: string,
    action: FileActions,
    actionExpiresSec: number,
  ): Promise<string> {
    const writeParams = {
      Bucket: bucketName,
      Expires: actionExpiresSec,
      Key: fileKey,
    } as any;

    return new Promise((resolve, reject) => {
      this.client.getSignedUrl(action, writeParams, (err, url) => {
        if (err) {
          return reject(err);
        }
        return resolve(url);
      });
    });
  }

  async getSignedUrlForPutObject(
    bucketName: string,
    fileKey: string,
    expiresSec = 3600,
  ): Promise<string> {
    await this.createBucketIfNotExist(bucketName);
    return this.getSignedUrl(bucketName, fileKey, 'putObject', expiresSec);
  }

  async getSignedUrlForGetObject(
    bucketName: string,
    fileKey: string,
    expiresSec = 3600,
  ): Promise<string> {
    return this.getSignedUrl(bucketName, fileKey, 'getObject', expiresSec);
  }

  async createBucketIfNotExist(bucketName: string): Promise<void> {
    new Promise((resolve, reject) => {
      this.client.createBucket(
        {
          Bucket: bucketName,
        },
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        },
      );
    });
  }

  async deleteObject(fileKey: string, bucketName: string): Promise<any> {
    new Promise((resolve, reject) => {
      this.client.deleteObject(
        {
          Bucket: bucketName,
          Key: fileKey,
        },
        (err, res) => {
          if (err) {
            return reject(err);
          }
          return resolve(res);
        },
      );
    });
  }
}
