import {
  ArgumentsHost,
  Catch,
  HttpException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';

const INTERNAL_SERVER_ERROR_MESSAGE =
  'Something went wrong. Please try again later';

@Catch()
export class LoggerExceptionFilter extends BaseExceptionFilter {
  private readonly logger = new Logger(LoggerExceptionFilter.name);

  catch(exception: unknown, host: ArgumentsHost) {
    if (exception instanceof HttpException) {
      const status = exception.getStatus();
      if (status >= 500) {
        this.logger.error(exception, exception.getResponse());
        exception = new InternalServerErrorException(
          INTERNAL_SERVER_ERROR_MESSAGE,
        );
      } else if (status >= 400) {
        this.logger.warn(exception.name, exception.getResponse());
      } else {
        this.logger.log(exception, exception.getResponse());
      }
    } else {
      this.logger.error(exception);
      exception = new InternalServerErrorException(
        INTERNAL_SERVER_ERROR_MESSAGE,
      );
    }

    super.catch(exception, host);
  }
}
