import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { ObjectId } from 'mongodb';

/**
 * Defines the pipe for MongoDB ObjectId validation and transformation
 */
@Injectable()
export class ParseMongoIdPipe implements PipeTransform<string, ObjectId> {
  /**
   * Validates and transforms a value to a MongoDB ObjectID
   *
   * @throws BadRequestException if the validation fails
   *
   * @param value - The value to validate and transform
   * @returns The MongoDB ObjectID
   */
  public transform(value: string): ObjectId {
    try {
      return ObjectId.createFromHexString(value);
    } catch (error) {
      throw new BadRequestException('Validation failed (objectId is expected)');
    }
  }
}
