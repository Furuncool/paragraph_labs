import { ExposeOptions, Transform } from 'class-transformer';

/**
 * Transform string to enum value.
 * @example
 * ```
 * enum TestEnum {
 *   key = 'value'
 * };
 * class TestClass {
 *   @StringToEnum(TestEnum)
 *   property: TestEnum;
 * }
 * const instance = new TestClass({ property: 'key'})
 * // => {
 *   property: 'value'
 * }
 */
export function StringToEnum(_enum: object, options?: ExposeOptions) {
  return (target: any, propertyKey: string) => {
    Transform(({ value }: { value: string }) => _enum[value] || value, options)(
      target,
      propertyKey,
    );
  };
}
