import { ExposeOptions, Transform } from 'class-transformer';
import { escapeStringRegexp } from '../utils';

/**
 * Transform value to escaped RegEx.
 * @description You can also use this to escape a string that is inserted into the middle of a regex, for example, into a character class.
 */
export function EscapeRegex(options?: ExposeOptions) {
  return (target: any, propertyKey: string) => {
    Transform(
      ({ value }: { value: string }) => escapeStringRegexp(value) || '',
      options,
    )(target, propertyKey);
  };
}
