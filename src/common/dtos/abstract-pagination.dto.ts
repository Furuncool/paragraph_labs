import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { IsDefined, Max, Min } from 'class-validator';
import { plainToInstance, Type } from 'class-transformer';

export class PaginationQueryDto {
  @ApiProperty({ default: 0 })
  @IsDefined()
  @Type(() => Number)
  @Min(0)
  skip: number;

  @ApiProperty({ default: 10 })
  @IsDefined()
  @Type(() => Number)
  @Min(1)
  @Max(50)
  limit: number;
}

export class PaginationMeta {
  currentCount: number;

  currentPage: number;

  totalCount: number;

  totalPages: number;

  countPerPage: number;

  hasMore: boolean;

  static create(
    currentCount: number,
    totalCount: number,
    skip: number,
    limit: number,
  ): PaginationMeta {
    const currentPage = Math.ceil((skip || 1) / limit);
    const totalPages = Math.ceil(totalCount / limit);
    const countPerPage = limit;
    const hasMore = totalCount > currentPage * limit;

    return plainToInstance(PaginationMeta, {
      currentCount,
      totalCount,
      currentPage,
      totalPages,
      countPerPage,
      hasMore,
    });
  }
}

export class PaginationMetaResDto implements PaginationMeta {
  @ApiProperty()
  currentCount: number;

  @ApiProperty()
  currentPage: number;

  @ApiProperty()
  totalCount: number;

  @ApiProperty()
  totalPages: number;

  @ApiProperty()
  countPerPage: number;

  @ApiProperty()
  hasMore: boolean;
}

export interface IPaginationResDto<T> extends PaginationMeta {
  data: T[];
}

export interface IMongoPaginationDto<T extends Document>
  extends PaginationMeta {
  data: T[];
}

export const PaginationMetaProperties: (keyof PaginationMeta)[] = [
  'totalCount',
  'totalPages',
  'currentPage',
  'currentCount',
  'countPerPage',
  'hasMore',
];
