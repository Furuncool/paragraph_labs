import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class BaseEntityResDto {
  @ApiProperty({ example: '507f1f77bcf86cd799439011' })
  @Type(() => String)
  _id: string;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
