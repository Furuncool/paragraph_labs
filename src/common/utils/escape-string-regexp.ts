/**
 * Escape RegExp special characters.
 * @description You can also use this to escape a string that is inserted into the middle of a regex, for example, into a character class.
 * @example
 * const escapedString = escapeStringRegexp('How much $ for a 🦄?');
 * //=> 'How much \\$ for a 🦄\\?'
 * */
export function escapeStringRegexp(str: string): string {
  return str.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&').replace(/-/g, '\\x2d');
}
