/**
 * Pick properties from instance
 */
export function pickObject<Obj extends object, Keys extends keyof Obj>(
  obj: Obj,
  keys: Keys[] | readonly Keys[],
): Pick<Obj, Keys> {
  const result: Partial<Pick<Obj, Keys>> = {};
  keys.forEach((key) => (result[key] = obj[key]));
  return result as Pick<Obj, Keys>;
}
