import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UploadFileRequestDto {
  @ApiProperty()
  @IsString()
  fileName: string;
}

export class GetFileQueryDto {
  @ApiProperty()
  @IsString()
  fileKey: string;
}
