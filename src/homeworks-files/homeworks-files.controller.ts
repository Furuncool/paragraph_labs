import { Body, Controller, Get, Param, Put, Query } from "@nestjs/common";
import { HomeworksFilesService } from './homeworks-files.service';
import { ApiTags } from '@nestjs/swagger';
import { GetFileQueryDto, UploadFileRequestDto } from './homeworks-files.dto';

@ApiTags('homeworks-files')
@Controller('homeworks-files')
export class HomeworksFilesController {
  constructor(private readonly filesService: HomeworksFilesService) {}

  @Get(':homeworkId/file-signed-url')
  async getSignedUrlForGetPdf(
    @Param('homeworkId') homeworkId: string,
    @Query() query: GetFileQueryDto,
  ) {
    return this.filesService.getSignedUrlForGetPdf(homeworkId, query);
  }

  @Put(':homeworkId/file-signed-url')
  async getSignedUrlForSavePdf(
    @Param('homeworkId') homeworkId: string,
    @Body() dto: UploadFileRequestDto,
  ) {
    return this.filesService.getSignedUrlForSavePdf(homeworkId, dto);
  }
}
