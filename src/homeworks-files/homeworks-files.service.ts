import { Injectable } from '@nestjs/common';
import { S3Service } from '../common/services';
import { ConfigService } from '@nestjs/config';
import { GetFileQueryDto, UploadFileRequestDto } from './homeworks-files.dto';

@Injectable()
export class HomeworksFilesService {
  constructor(
    private readonly configService: ConfigService,
    private readonly s3Service: S3Service,
  ) {}

  async getSignedUrlForGetPdf(
    homeworkId: string,
    dto: GetFileQueryDto,
  ): Promise<string> {
    return this.s3Service.getSignedUrlForGetObject(homeworkId, dto.fileKey);
  }

  async getSignedUrlForSavePdf(
    homeworkId: string,
    dto: UploadFileRequestDto,
  ): Promise<string> {
    return this.s3Service.getSignedUrlForPutObject(homeworkId, dto.fileName);
  }
}
