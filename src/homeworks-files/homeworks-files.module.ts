import { Module } from '@nestjs/common';
import { HomeworksFilesService } from './homeworks-files.service';
import { HomeworksFilesController } from './homeworks-files.controller';
import { S3Service } from '../common/services';

@Module({
  providers: [HomeworksFilesService, S3Service],
  controllers: [HomeworksFilesController],
})
export class HomeworksFilesModule {}
