import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import {
  INestApplication,
  Logger,
  LogLevel,
  ValidationPipe,
} from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ENVIRONMENT } from './common';

const customSwaggerOptions: SwaggerCustomOptions = {
  swaggerOptions: {
    displayRequestDuration: true,
    filter: true,
    syntaxHighlight: { activate: true, theme: 'agate' },
    tryItOutEnabled: true,
  },
};

const useSwagger = (app: INestApplication) => {
  const options = new DocumentBuilder()
    .setTitle('HOMEWORKS API')
    .setDescription('The homeworks API description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document, customSwaggerOptions);
};

async function bootstrap() {
  const ENV = process.env.ENVIRONMENT;
  const PORT = process.env.PORT;

  const logLevels: LogLevel[] =
    ENV === ENVIRONMENT.PRODUCTION ? ['log', 'warn', 'error'] : ['debug'];

  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: logLevels,
  });

  useSwagger(app);
  app.enableCors({ origin: '*' });
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));

  await app.listen(PORT, () => {
    Logger.log(`Running on port ${PORT}`, 'NestApplication');
    Logger.log(`Environment ${ENV}`, 'NestApplication');
  });
}

bootstrap();
