import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { HomeworkList } from '../homework-list.entity';
import { plainToInstance } from 'class-transformer';
import {
  BaseEntityResDto,
  IMongoPaginationDto,
  IPaginationResDto,
  PaginationMetaProperties,
  PaginationMetaResDto,
  pickObject,
} from 'src/common';
import { HomeworkResDto } from 'src/homeworks/dtos';

export class HomeworkListResDto extends BaseEntityResDto {
  @ApiProperty({ example: 'Название' })
  title: string;

  @ApiPropertyOptional({ example: 'Описание' })
  description?: string;

  @ApiProperty({
    type: HomeworkResDto,
    isArray: true,
  })
  homeworks: HomeworkResDto[];

  static create(entity: HomeworkList): HomeworkListResDto {
    const keys: (keyof HomeworkList)[] = ['_id', 'title', 'description'];
    const homeworks = entity.homeworks.map(HomeworkResDto.create);
    return plainToInstance(HomeworkListResDto, {
      ...pickObject(entity, keys),
      homeworks,
    });
  }
}

export class HomeworkListManyResDto
  extends PaginationMetaResDto
  implements IPaginationResDto<HomeworkListResDto>
{
  @ApiProperty({
    type: HomeworkListResDto,
    isArray: true,
  })
  data: HomeworkListResDto[];

  static create(
    paginated: IMongoPaginationDto<HomeworkList>,
  ): HomeworkListManyResDto {
    const data = paginated.data.map(HomeworkListResDto.create);
    return plainToInstance(HomeworkListManyResDto, {
      ...pickObject(paginated, PaginationMetaProperties),
      data,
    });
  }
}
