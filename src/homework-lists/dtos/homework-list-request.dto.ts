import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsEnum,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import {
  EscapeRegex,
  PaginationQueryDto,
  SortDirection,
  StringToEnum,
} from 'src/common';
import { HomeworkListSortFields } from '../homeworks-lists.types';

export class CreateHomeworkListDto {
  @ApiProperty({ example: 'Название' })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiPropertyOptional({ example: 'Описание' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  description?: string;

  @ApiPropertyOptional({
    type: String,
    example: ['507f1f77bcf86cd799439011'],
    isArray: true,
  })
  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  homeworks?: string[];
}

export class UpdateHomeworkListDto extends PartialType(CreateHomeworkListDto) {}

export class ReplaceHomeworkListDto extends CreateHomeworkListDto {}

export class FindManyHomeworkListQueryDto extends PaginationQueryDto {
  @ApiPropertyOptional({
    enum: Object.keys(HomeworkListSortFields),
  })
  @IsOptional()
  @StringToEnum(HomeworkListSortFields)
  @IsEnum(HomeworkListSortFields)
  sortField: HomeworkListSortFields;

  @ApiPropertyOptional({
    enum: Object.keys(SortDirection),
  })
  @IsOptional()
  @StringToEnum(SortDirection)
  @IsEnum(SortDirection)
  sortDirection: SortDirection;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @EscapeRegex()
  searchQuery: string;
}
