import { AbstractMongoRepository, IMongoPaginationDto } from 'src/common';
import { HomeworkList } from './homework-list.entity';
import { Model } from 'mongoose';
import { Injectable, Provider } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FindManyHomeworkListQueryDto } from './dtos';

@Injectable()
export class HomeworkListsRepository extends AbstractMongoRepository<HomeworkList> {
  constructor(
    @InjectModel(HomeworkList.name)
    private readonly homeworkListModel: Model<HomeworkList>,
  ) {
    super(homeworkListModel);
  }

  async paginateWithFilters(
    query: FindManyHomeworkListQueryDto,
  ): Promise<IMongoPaginationDto<HomeworkList>> {
    const { skip, limit, sortDirection, sortField, searchQuery } = query;
    return this.find(
      {
        title: { $regex: searchQuery || '', $options: 'i' },
      },
      {
        skip,
        limit,
        sort: { [`${sortField}`]: sortDirection },
      },
    );
  }
}

export const HomeworkListsRepositoryProvider: Provider = {
  provide: HomeworkListsRepository,
  useValue: HomeworkListsRepository,
};
