import { NotFoundException } from '@nestjs/common';

export class HomeworkListNotFoundException extends NotFoundException {
  constructor(homeworkListId?: string) {
    if (homeworkListId) {
      super(`Homework list with id: ${homeworkListId} not found`);
    }
    super(`Homework list not found`);
  }
}
