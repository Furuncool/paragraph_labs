import { ConflictException } from '@nestjs/common';

export class HomeworkListAlreadyExistException extends ConflictException {
  constructor(message?: string) {
    super(message || 'This homework list already exist');
  }
}
