import { Injectable } from '@nestjs/common';
import { HomeworkList } from './homework-list.entity';
import {
  CreateHomeworkListDto,
  FindManyHomeworkListQueryDto,
  UpdateHomeworkListDto,
} from './dtos';
import {
  HomeworkListAlreadyExistException,
  HomeworkListNotFoundException,
} from './exceptions';
import { HomeworkListsRepository } from './homework-lists.repository';
import { IMongoPaginationDto } from 'src/common';

@Injectable()
export class HomeworkListsService {
  constructor(
    private readonly homeworkListsRepository: HomeworkListsRepository,
  ) {}

  async findOne(id: string): Promise<HomeworkList> {
    const homeworkList = await this.homeworkListsRepository.findOne(
      { _id: id },
      {
        populate: 'homeworks',
      },
    );
    if (!homeworkList) {
      throw new HomeworkListNotFoundException(id);
    }
    return homeworkList;
  }

  async findAll(
    query: FindManyHomeworkListQueryDto,
  ): Promise<IMongoPaginationDto<HomeworkList>> {
    return this.homeworkListsRepository.paginateWithFilters(query);
  }

  async create(dto: CreateHomeworkListDto): Promise<HomeworkList> {
    const existHomeworkList = await this.homeworkListsRepository.findOne({
      title: dto.title,
    });
    if (existHomeworkList) {
      throw new HomeworkListAlreadyExistException();
    }
    return this.homeworkListsRepository.create(dto, 'homeworks');
  }

  async update(
    id: string,
    dto: UpdateHomeworkListDto,
    overwrite = false,
  ): Promise<HomeworkList> {
    const existHomeworkList = await this.homeworkListsRepository.findOne({
      _id: { $ne: id },
      title: dto.title,
    });
    if (existHomeworkList) {
      throw new HomeworkListAlreadyExistException();
    }

    const updatedHomeworkList = await this.homeworkListsRepository.update(
      { _id: id },
      dto,
      { populate: 'homeworks', overwrite },
    );
    if (!updatedHomeworkList) {
      throw new HomeworkListNotFoundException(id);
    }
    return updatedHomeworkList;
  }

  async replace(id: string, dto: UpdateHomeworkListDto): Promise<HomeworkList> {
    return this.update(id, dto, true);
  }

  async delete(id: string): Promise<HomeworkList> {
    const deletedHomeworkList = await this.homeworkListsRepository.delete({
      _id: id,
    });
    if (!deletedHomeworkList) {
      throw new HomeworkListNotFoundException(id);
    }
    return deletedHomeworkList;
  }
}
