import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Homework } from 'src/homeworks/homework.entity';
import { BaseEntity } from 'src/common';

@Schema({ timestamps: true })
export class HomeworkList extends BaseEntity {
  @Prop({ isRequired: true, unique: true, trim: true })
  title: string;

  @Prop({ trim: true, default: null })
  description: string | null;

  @Prop({ default: null })
  acceptance: string | null;

  @Prop({
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: Homework.name,
      },
    ],
  })
  homeworks?: Homework[];
}

export const HomeworkListSchema = SchemaFactory.createForClass(HomeworkList);
