import { Module } from '@nestjs/common';
import { HomeworkListsController } from './homework-lists.controller';
import { HomeworkListsService } from './homework-lists.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HomeworkList, HomeworkListSchema } from './homework-list.entity';
import { HomeworkListsRepository } from './homework-lists.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: HomeworkList.name, schema: HomeworkListSchema },
    ]),
  ],
  controllers: [HomeworkListsController],
  providers: [HomeworkListsService, HomeworkListsRepository],
})
export class HomeworkListsModule {}
