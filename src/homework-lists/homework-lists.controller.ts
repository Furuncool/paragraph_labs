import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { HomeworkListsService } from './homework-lists.service';
import { ParseMongoIdPipe } from 'src/common';
import {
  CreateHomeworkListDto,
  FindManyHomeworkListQueryDto,
  HomeworkListManyResDto,
  HomeworkListResDto,
  ReplaceHomeworkListDto,
  UpdateHomeworkListDto,
} from './dtos';

@ApiTags('homework-lists')
@Controller('homework-lists')
export class HomeworkListsController {
  constructor(private readonly homeworkListsService: HomeworkListsService) {}

  @ApiOperation({
    summary: 'Retrieve one homework list by id',
  })
  @ApiOkResponse({
    type: HomeworkListResDto,
  })
  @Get(':id')
  async findOneHomeworkList(
    @Param('id', ParseMongoIdPipe) id: string,
  ): Promise<HomeworkListResDto> {
    const homeworkList = await this.homeworkListsService.findOne(id);
    return HomeworkListResDto.create(homeworkList);
  }

  @ApiOperation({
    summary: 'Retrieve all homework lists',
  })
  @ApiOkResponse({
    type: HomeworkListManyResDto,
  })
  @Get()
  async findAllHomeworkList(
    @Query() query: FindManyHomeworkListQueryDto,
  ): Promise<HomeworkListManyResDto> {
    const paginated = await this.homeworkListsService.findAll(query);
    return HomeworkListManyResDto.create(paginated);
  }

  @ApiOperation({
    summary: 'Create homework list',
  })
  @ApiOkResponse({
    type: HomeworkListResDto,
  })
  @Post()
  async createHomeworkList(
    @Body() dto: CreateHomeworkListDto,
  ): Promise<HomeworkListResDto> {
    const createdHomeworkList = await this.homeworkListsService.create(dto);
    return HomeworkListResDto.create(createdHomeworkList);
  }

  @ApiOperation({
    summary: 'Update homework list by id',
  })
  @ApiOkResponse({
    type: HomeworkListResDto,
  })
  @Patch(':id')
  async updateHomeworkList(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() dto: UpdateHomeworkListDto,
  ): Promise<HomeworkListResDto> {
    const updatedHomeworkList = await this.homeworkListsService.update(id, dto);
    return HomeworkListResDto.create(updatedHomeworkList);
  }

  @ApiOperation({
    summary: 'Replace homework list by id',
  })
  @ApiOkResponse({
    type: HomeworkListResDto,
  })
  @Put(':id')
  async replaceHomeworkList(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() dto: ReplaceHomeworkListDto,
  ): Promise<HomeworkListResDto> {
    const updatedHomeworkList = await this.homeworkListsService.replace(
      id,
      dto,
    );
    return HomeworkListResDto.create(updatedHomeworkList);
  }

  @ApiOperation({
    summary: 'Delete homework list by id',
  })
  @ApiOkResponse({
    type: HomeworkListResDto,
  })
  @Delete(':id')
  async deleteHomeworkList(
    @Param('id', ParseMongoIdPipe) id: string,
  ): Promise<HomeworkListResDto> {
    const deletedHomeworkList = await this.homeworkListsService.delete(id);
    return HomeworkListResDto.create(deletedHomeworkList);
  }
}
