export enum HomeworkListSortFields {
  BY_DEFAULT = '_id',
  BY_CREATED_AT = 'createdAt',
  BY_UPDATED_AT = 'updatedAt',
  BY_TITLE = 'title',
}
