import * as Joi from 'joi';
import { ENVIRONMENT } from 'src/common';
import * as process from "process";

const validationSchema = Joi.object({
  ENVIRONMENT: Joi.string()
    .valid(...Object.values(ENVIRONMENT))
    .required(),
  PORT: Joi.number().port().required(),

  // database
  MONGO_USERNAME: Joi.string().required(),
  MONGO_PASSWORD: Joi.string().required(),
  MONGO_DATABASE: Joi.string().required(),
  MONGO_HOST: Joi.string().required(),

  // s3
  S3_ACCESS_KEY: Joi.string().required(),
  S3_SECRET_ACCESS_KEY: Joi.string().required(),
  S3_REGION: Joi.string().required(),
  S3_ENDPOINT: Joi.string().required(),
});

const configuration = () => ({
  environment: process.env.ENVIRONMENT,
  port: parseInt(process.env.PORT, 10),
  database: {
    username: process.env.MONGO_USERNAME,
    password: parseInt(process.env.MONGO_PASSWORD, 10),
    name: process.env.MONGO_DATABASE,
    host: process.env.MONGO_HOST,
  },
  s3: {
    key: process.env.S3_ACCESS_KEY,
    secret: process.env.S3_SECRET_ACCESS_KEY,
    region: process.env.S3_REGION,
    endpoint: process.env.S3_ENDPOINT,
  },
});

const validationOptions = {
  abortEarly: true,
};

export const config = {
  configuration,
  validationOptions,
  validationSchema,
};
