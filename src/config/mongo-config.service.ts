import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  MongooseModuleOptions,
  MongooseOptionsFactory,
} from '@nestjs/mongoose';
import { ENVIRONMENT } from 'src/common';
import mongoose from 'mongoose';

@Injectable()
export class MongoConfigService implements MongooseOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  createMongooseOptions(): MongooseModuleOptions {
    const user = this.configService.get<string>('MONGO_USERNAME');
    const pass = this.configService.get<string>('MONGO_PASSWORD');
    const host = this.configService.get<string>('MONGO_HOST');
    const env = this.configService.get<ENVIRONMENT>('ENVIRONMENT');

    if (env !== ENVIRONMENT.PRODUCTION) {
      mongoose.set('debug', true);
    }

    const uri = `mongodb://${user}:${pass}@${host}`;

    return {
      user,
      pass,
      uri,
      dbName: this.configService.get<string>('MONGO_DATABASE'),
      port: this.configService.get<number>('database.port'),
    };
  }
}
