import { ConflictException } from '@nestjs/common';

export class HomeworkAlreadyExistException extends ConflictException {
  constructor(message?: string) {
    super(message || 'This homework already exist');
  }
}
