import { NotFoundException } from '@nestjs/common';

export class HomeworkNotFoundException extends NotFoundException {
  constructor(homeworkId?: string) {
    if (homeworkId) {
      super(`Homework with id: ${homeworkId} not found`);
    }
    super(`Homework not found`);
  }
}
