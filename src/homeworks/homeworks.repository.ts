import { Injectable, Provider } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Homework } from './homework.entity';
import { AbstractMongoRepository, IMongoPaginationDto } from 'src/common';
import { FindManyHomeworksQueryDto } from './dtos';

@Injectable()
export class HomeworksRepository extends AbstractMongoRepository<Homework> {
  constructor(
    @InjectModel(Homework.name) private homeworkModel: Model<Homework>,
  ) {
    super(homeworkModel);
  }

  async paginateWithFilters(
    query: FindManyHomeworksQueryDto,
  ): Promise<IMongoPaginationDto<Homework>> {
    const { skip, limit, sortDirection, sortField, searchQuery } = query;
    return this.find(
      {
        title: { $regex: searchQuery || '', $options: 'i' },
      },
      {
        skip,
        limit,
        sort: { [`${sortField}`]: sortDirection },
      },
    );
  }
}

export const HomeworksRepositoryProvider: Provider = {
  provide: HomeworksRepository,
  useValue: HomeworksRepository,
};
