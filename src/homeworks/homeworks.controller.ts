import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ParseMongoIdPipe } from 'src/common';
import {
  CreateHomeworkDto,
  FindManyHomeworksQueryDto,
  HomeworkManyResDto,
  HomeworkResDto,
  ReplaceHomeworkDto,
  UpdateHomeworkDto,
} from './dtos';
import { HomeworksService } from './homeworks.service';

@ApiTags('homeworks')
@Controller('homeworks')
export class HomeworksController {
  constructor(private readonly homeworksService: HomeworksService) {}

  @ApiOperation({
    summary: 'Retrieve one homework by id',
  })
  @ApiOkResponse({
    type: HomeworkResDto,
  })
  @Get(':id')
  async findOneHomework(
    @Param('id', ParseMongoIdPipe) id: string,
  ): Promise<HomeworkResDto> {
    const homework = await this.homeworksService.findOne(id);
    return HomeworkResDto.create(homework);
  }

  @ApiOperation({
    summary: 'Retrieve all homeworks',
  })
  @ApiOkResponse({
    type: HomeworkManyResDto,
  })
  @Get()
  async findAllHomeworks(
    @Query() query: FindManyHomeworksQueryDto,
  ): Promise<HomeworkManyResDto> {
    const paginated = await this.homeworksService.findAll(query);
    return HomeworkManyResDto.create(paginated);
  }

  @ApiOperation({
    summary: 'Create one homework',
  })
  @ApiCreatedResponse({
    type: HomeworkResDto,
  })
  @Post()
  async createHomework(
    @Body() dto: CreateHomeworkDto,
  ): Promise<HomeworkResDto> {
    const createdHomework = await this.homeworksService.create(dto);
    return HomeworkResDto.create(createdHomework);
  }

  @ApiOperation({
    summary: 'Update one homework by id',
  })
  @ApiOkResponse({
    type: HomeworkResDto,
  })
  @Patch(':id')
  async updateHomework(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() dto: UpdateHomeworkDto,
  ): Promise<HomeworkResDto> {
    const updatedHomework = await this.homeworksService.update(id, dto);
    return HomeworkResDto.create(updatedHomework);
  }

  @ApiOperation({
    summary: 'Replace one homework by id',
  })
  @ApiOkResponse({
    type: HomeworkResDto,
  })
  @Put(':id')
  async replaceHomework(
    @Param('id', ParseMongoIdPipe) id: string,
    @Body() dto: ReplaceHomeworkDto,
  ): Promise<HomeworkResDto> {
    const updatedHomework = await this.homeworksService.replace(id, dto);
    return HomeworkResDto.create(updatedHomework);
  }

  @ApiOperation({
    summary: 'Delete one homework by id',
  })
  @ApiOkResponse({
    type: HomeworkResDto,
  })
  @Delete(':id')
  async deleteHomework(
    @Param('id', ParseMongoIdPipe) id: string,
  ): Promise<HomeworkResDto> {
    const deletedHomework = await this.homeworksService.delete(id);
    return HomeworkResDto.create(deletedHomework);
  }
}
