import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { BaseEntity } from 'src/common';
import { Paragraph } from './dtos';

@Schema({ timestamps: true, collection: 'homeworks' })
export class Homework extends BaseEntity {
  @Prop({ required: true, unique: true, trim: true })
  title: string;

  @Prop({ trim: true, default: null })
  description: string | null;

  @Prop()
  paragraphs: Paragraph[];
}

export const HomeworkSchema = SchemaFactory.createForClass(Homework);
