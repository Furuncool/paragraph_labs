export enum HomeworkSortFields {
  BY_DEFAULT = '_id',
  BY_CREATED_AT = 'createdAt',
  BY_UPDATED_AT = 'updatedAt',
  BY_TITLE = 'title',
}

export enum ParagraphTypes {
  MAIN = 'MAIN',
  CUSTOM = 'CUSTOM',
  CRITERIA = 'CRITERIA',
  ACCEPTANCE = 'ACCEPTANCE',
}

export type ParagraphContentOptions = Record<string, any>;
