import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Homework } from '../homework.entity';
import { plainToInstance } from 'class-transformer';
import {
  BaseEntityResDto,
  IMongoPaginationDto,
  IPaginationResDto,
  PaginationMetaProperties,
  PaginationMetaResDto,
  pickObject,
} from 'src/common';
import { ParagraphResDto } from './paragraph.dto';

export class HomeworkResDto extends BaseEntityResDto {
  @ApiProperty({ example: 'Название' })
  title: string;

  @ApiPropertyOptional({ example: 'Описание' })
  description?: string;

  @ApiPropertyOptional({
    type: () => ParagraphResDto,
    isArray: true,
  })
  paragraphs?: ParagraphResDto[];

  static create(entity: Homework): HomeworkResDto {
    const keys: (keyof Homework)[] = [
      '_id',
      'title',
      'description',
      'createdAt',
      'updatedAt',
    ];
    const paragraphs = entity.paragraphs?.map(ParagraphResDto.create);
    return plainToInstance(HomeworkResDto, {
      ...pickObject(entity, keys),
      paragraphs,
    });
  }
}

export class HomeworkManyResDto
  extends PaginationMetaResDto
  implements IPaginationResDto<HomeworkResDto>
{
  @ApiProperty({
    type: () => HomeworkResDto,
    isArray: true,
  })
  data: HomeworkResDto[];

  static create(paginated: IMongoPaginationDto<Homework>): HomeworkManyResDto {
    const data = paginated.data.map(HomeworkResDto.create);
    return plainToInstance(HomeworkManyResDto, {
      ...pickObject(paginated, PaginationMetaProperties),
      data,
    });
  }
}
