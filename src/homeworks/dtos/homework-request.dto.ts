import { ApiProperty, ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { EscapeRegex, PaginationQueryDto, SortDirection } from 'src/common';
import { HomeworkSortFields } from '../homeworks.types';
import { StringToEnum } from 'src/common/transforms/';
import { CreateParagraphDto } from './paragraph.dto';
import { Type } from 'class-transformer';

export class CreateHomeworkDto {
  @ApiProperty({ example: 'Название' })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiPropertyOptional({ example: 'Описание' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  description?: string;

  @ApiPropertyOptional({
    type: () => CreateParagraphDto,
    isArray: true,
  })
  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateParagraphDto)
  paragraphs?: CreateParagraphDto[];
}

export class UpdateHomeworkDto extends PartialType(CreateHomeworkDto) {}

export class ReplaceHomeworkDto extends CreateHomeworkDto {}

export class FindManyHomeworksQueryDto extends PaginationQueryDto {
  @ApiPropertyOptional({
    enum: Object.keys(HomeworkSortFields),
  })
  @IsOptional()
  @StringToEnum(HomeworkSortFields)
  @IsEnum(HomeworkSortFields)
  sortField: HomeworkSortFields;

  @ApiPropertyOptional({
    enum: Object.keys(SortDirection),
  })
  @IsOptional()
  @StringToEnum(SortDirection)
  @IsEnum(SortDirection)
  sortDirection: SortDirection;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @EscapeRegex()
  searchQuery: string;
}
