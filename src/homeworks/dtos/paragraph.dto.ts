import { ParagraphContentOptions, ParagraphTypes } from '../homeworks.types';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsArray,
  IsDefined,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
  Min,
  ValidateNested,
} from 'class-validator';
import { pickObject } from 'src/common';
import { plainToInstance, Type } from 'class-transformer';
import { ObjectId } from 'mongodb';

export interface Paragraph {
  _id: string;

  title?: string | null;

  type: ParagraphTypes;

  content: ParagraphContent[];
}

export interface ParagraphContent {
  _id: string;

  title?: string;

  value: string;

  nestingLevel: number;

  options?: ParagraphContentOptions;
}

export class ParagraphContentResDto implements ParagraphContent {
  @ApiProperty()
  _id: string;

  @ApiPropertyOptional({ example: 'Решение' })
  title?: string;

  @ApiProperty({ example: '2+2' })
  value: string;

  @ApiProperty()
  nestingLevel: number;

  @ApiPropertyOptional({
    type: 'object',
    additionalProperties: {},
  })
  options?: ParagraphContentOptions;

  static create(entity: ParagraphContent): ParagraphContentResDto {
    return plainToInstance(ParagraphContentResDto, {
      _id: new ObjectId(),
      ...pickObject(entity, ['title', 'value', 'nestingLevel', 'options']),
    });
  }
}

export class ParagraphResDto implements Paragraph {
  @ApiProperty()
  _id: string;

  @ApiPropertyOptional({ example: 'Критерий приемки' })
  title: string;

  @ApiProperty({
    enum: ParagraphTypes,
  })
  type: ParagraphTypes;

  @ApiProperty({
    type: () => ParagraphContentResDto,
    isArray: true,
  })
  content: ParagraphContentResDto[];

  static create(entity: Paragraph): ParagraphResDto {
    const content = entity.content.map(ParagraphContentResDto.create);
    return plainToInstance(ParagraphResDto, {
      _id: new ObjectId(),
      ...pickObject(entity, ['title', 'type']),
      content,
    });
  }
}

export class CreateParagraphContent {
  @ApiProperty({ example: 'Решение' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  title?: string;

  @ApiProperty({ example: '2+2' })
  @IsNotEmpty()
  @IsString()
  value: string;

  @ApiProperty()
  @IsInt()
  @Min(0)
  nestingLevel: number;

  @ApiPropertyOptional({
    type: 'object',
    additionalProperties: {},
  })
  @IsOptional()
  @IsDefined()
  @IsObject()
  options?: ParagraphContentOptions;
}

export class CreateParagraphDto {
  @ApiPropertyOptional({ example: 'Критерий приемки' })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  title?: string;

  @ApiProperty({
    enum: ParagraphTypes,
    example: ParagraphTypes.CUSTOM,
  })
  @IsEnum(ParagraphTypes)
  type: ParagraphTypes;

  @ApiPropertyOptional({
    type: () => CreateParagraphContent,
    isArray: true,
  })
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateParagraphContent)
  content: CreateParagraphContent[];
}
