import { Module } from '@nestjs/common';
import { HomeworksController } from './homeworks.controller';
import { HomeworksService } from './homeworks.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Homework, HomeworkSchema } from './homework.entity';
import { HomeworksRepository } from './homeworks.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Homework.name, schema: HomeworkSchema },
    ]),
  ],
  controllers: [HomeworksController],
  providers: [HomeworksService, HomeworksRepository],
})
export class HomeworksModule {}
