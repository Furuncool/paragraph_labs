import { Injectable } from '@nestjs/common';
import {
  HomeworkAlreadyExistException,
  HomeworkNotFoundException,
} from './exceptions';
import { Homework } from './homework.entity';
import {
  CreateHomeworkDto,
  FindManyHomeworksQueryDto,
  UpdateHomeworkDto,
} from './dtos';
import { IMongoPaginationDto } from 'src/common';
import { HomeworksRepository } from './homeworks.repository';

@Injectable()
export class HomeworksService {
  constructor(private readonly homeworksRepository: HomeworksRepository) {}

  async findOne(id: string): Promise<Homework> {
    const homework = await this.homeworksRepository.findOne({ _id: id });
    if (!homework) {
      throw new HomeworkNotFoundException(id);
    }
    return homework;
  }

  async findAll(
    query: FindManyHomeworksQueryDto,
  ): Promise<IMongoPaginationDto<Homework>> {
    return this.homeworksRepository.paginateWithFilters(query);
  }

  async create(dto: CreateHomeworkDto): Promise<Homework> {
    const existHomework = await this.homeworksRepository.findOne({
      title: dto.title,
    });
    if (existHomework) {
      throw new HomeworkAlreadyExistException();
    }
    return this.homeworksRepository.create(dto);
  }

  async update(
    id: string,
    dto: UpdateHomeworkDto,
    overwrite = false,
  ): Promise<Homework> {
    const existHomework = await this.homeworksRepository.findOne({
      _id: { $ne: id },
      title: dto.title,
    });
    if (existHomework) {
      throw new HomeworkAlreadyExistException();
    }

    const updatedHomework = await this.homeworksRepository.update(
      { _id: id },
      dto,
      { overwrite },
    );
    if (!updatedHomework) {
      throw new HomeworkNotFoundException(id);
    }
    return updatedHomework;
  }

  async replace(id: string, dto: UpdateHomeworkDto): Promise<Homework> {
    return this.update(id, dto, true);
  }

  async delete(id: string): Promise<Homework> {
    const deletedHomework = await this.homeworksRepository.delete({ _id: id });
    if (!deletedHomework) {
      throw new HomeworkNotFoundException(id);
    }
    return deletedHomework;
  }
}
