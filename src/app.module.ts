import { Module } from '@nestjs/common';
import { HomeworksModule } from './homeworks/homeworks.module';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { CommonModule } from './common/common.module';
import { config, MongoConfigService } from './config';
import { HomeworkListsModule } from './homework-lists/homework-lists.module';
import { HealthModule } from './health/health.module';
import { APP_FILTER } from '@nestjs/core';
import { LoggerExceptionFilter } from './common';
import { HomeworksFilesModule } from './homeworks-files/homeworks-files.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config.configuration],
      validationSchema: config.validationSchema,
      validationOptions: config.validationOptions,
      envFilePath: ['.env'],
    }),
    MongooseModule.forRootAsync({
      useClass: MongoConfigService,
    }),
    CommonModule,
    HealthModule,
    HomeworksModule,
    HomeworkListsModule,
    HomeworksFilesModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: LoggerExceptionFilter,
    },
  ],
})
export class AppModule {}
